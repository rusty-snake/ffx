# Firejail profile for FFX
# This file is overwritten after every install/update of FFX
# Persistent local customizations
include FFX.local
# Persistent global definitions
include globals.local

ignore noexec ${HOME}

noblacklist ${HOME}/.cache/mozilla
noblacklist ${HOME}/.mozilla

blacklist /usr/libexec

include disable-shell.inc
include disable-X11.inc
include disable-xdg.inc

whitelist ${HOME}/.cache/mozilla/firefox
whitelist ${HOME}/.mozilla

include whitelist-runuser-common.inc
include whitelist-usr-share-common.inc

private-bin true
private-etc ca-certificates,crypto-policies,fonts,hosts,machine-id,pki,resolv.conf,ssl

dbus-user none
dbus-system none

env MOZ_ENABLE_WAYLAND=1

include firefox-common.profile
