# Firejail profile for FFX
# This file is overwritten after every install/update of FFX
# Persistent local customizations
include FFX.local
# Persistent global definitions
include globals.local

ignore noexec ${HOME}

noblacklist ${HOME}/.cache/mozilla
noblacklist ${HOME}/.mozilla

include disable-passwdmgr.inc
include disable-shell.inc
include disable-xdg.inc

whitelist ${HOME}/.cache/mozilla/firefox
whitelist ${HOME}/.mozilla

include whitelist-runuser-common.inc
include whitelist-usr-share-common.inc

# Breaks WebRTC
protocol unix,inet,inet6
ignore protocol

private-bin empty
private-etc ca-certificates,crypto-policies,fonts,hosts,machine-id,pki,resolv.conf,ssl

dbus-user none
dbus-system none

include firefox-common.profile
