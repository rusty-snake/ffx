# Moved to <https://codeberg.org/rusty-snake/ffx>

<br>


# FFX

Easily install all firefox release channels and use firejail to sandbox them.

## Getting started

```
$ git clone https://gitlab.com/rusty-snake/ffx.git
$ cd ffx
$ ./FFX.bash
```

### Uninstall

```
rm -rf "$HOME"/.FFX "$HOME"/.config/ffx.profile "$HOME"/.local/bin/firefox*latest "$HOME"/.local/share/applications/firefox*latest.desktop
```

## Features

 - GNOME-shell integration: Create a own folder in the applications-overview.
 - Add a policies.json which sanitizes on shutdown and installs uBlock origin.
 - Managed Storage Manifest for uBlock origin.
 - ffx.cfg (aka global user.js)
