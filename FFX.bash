#!/bin/bash

# Copyright © 2020,2021 rusty-snake
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#
# Begin
#

set -eo pipefail
trap "exit" INT TERM
trap "kill 0" EXIT

HERE="$(dirname "$0")"
BINDIR="$HOME/.local/bin"
FIREJAIL_DIR="$HOME/.config/firejail"
FFX_HOME="$HOME/.FFX"
LANG_CODE="de"
DOWNLOAD_URL="https://download.mozilla.org/?product=%PRODUCT%&os=linux64&lang=$LANG_CODE"
PRODUCT_CODES=(
	firefox-nightly-latest
	firefox-devedition-latest
	firefox-beta-latest
	firefox-latest
	firefox-esr-next-latest
	firefox-esr-latest
)

[ -d "$FFX_HOME" ] && { echo "[ Error ] Please delete your old FFX installation."; exit 1; }

mkdir -p "$FIREJAIL_DIR"
mkdir -p "${XDG_DATA_HOME:="$HOME"/.local/share}/applications"

#
# Main part
#

for product_code in "${PRODUCT_CODES[@]}"; do
	product_home="$FFX_HOME/$product_code"

	echo "[ Info ] Start downloading $product_code"
	mkdir -p "$product_home"
	(
		wget -q -O- "${DOWNLOAD_URL/\%PRODUCT\%/"$product_code"}" |
			tar -C "$product_home" -xj &&
			echo "[ Ok ] Downloaded $product_code" ||
			echo "[ Error ] Failed to download $product_code"
	)&

	sed \
		-e "s|@product_code@|$product_code|g" \
		-e "s|@product_home@|$product_home|g" \
		"$HERE/firefox.sh.in" > "$BINDIR/$product_code"
	chmod +x "$BINDIR/$product_code"
	echo "[ Ok ] Added start-script for $product_code"

	sed \
		-e "s|@product_code@|$product_code|g" \
		-e "s|@product_home@|$product_home|g" \
		"$HERE/firefox.desktop.in" > "$XDG_DATA_HOME/applications/$product_code.desktop"
	echo "[ Ok ] Added desktop-file for $product_code"

	mkdir -p "$product_home"/firefox/defaults/pref
	cat > "$product_home"/firefox/defaults/pref/autoconfig.js <<-EOF
	pref("general.config.filename", "ffx.cfg");
	pref("general.config.obscure_value", 0);
	EOF
	cp "$HERE/ffx.cfg" "$product_home"/firefox/ffx.cfg
	echo "[ Ok ] Added ffx.cfg to $product_code"

	mkdir -p "$product_home"/firefox/distribution
	sed "\#^[[:space:]]*//#d" "$HERE/policies.json" > "$product_home"/firefox/distribution/policies.json
	echo "[ Ok ] Added policies.json to $product_code"

	mkdir -p "$product_home"/.mozilla/managed-storage
	sed "\#^[[:space:]]*//#d" "$HERE/uBlock0@raymondhill.net.json" > "$product_home"/.mozilla/managed-storage/uBlock0@raymondhill.net.json
	echo "[ Ok ] Added managed storage manifest for µBlock origin to $product_code"
done

#
# FireJail
#

cp "$HERE/FFX.profile" "$FIREJAIL_DIR/FFX.profile"
echo "[ Ok ] Added firejail profile for ffx"

DISABLE_PROGRAMS_LOCAL="$FIREJAIL_DIR/disable-programs.local"
if ! grep -q "blacklist \${HOME}/.FFX" "$DISABLE_PROGRAMS_LOCAL"; then
	if [[ ! -e "$DISABLE_PROGRAMS_LOCAL" ]]; then
		echo "include \${CFG}/disable-programs.local" > "$DISABLE_PROGRAMS_LOCAL"
		echo "[ Ok ] Created disable-programs.local"
	fi
	echo "blacklist \${HOME}/.FFX" >> "$DISABLE_PROGRAMS_LOCAL"
	echo "[ Ok ] Added blacklist for ffx to disable-programs.local"
fi

#
# GNOME
#

if [[ "$XDG_SESSION_DESKTOP" == "gnome" ]]; then
	echo "[ Info ] Enable GNOME-app-folders integration"
	FC="$(gsettings get org.gnome.desktop.app-folders folder-children)"
	if [[ ! "$FC" =~ .*\'FFX\'.* ]]; then
		FC="$(python3 -c "fc=$FC;fc.append('FFX');print(fc)")"
		gsettings set org.gnome.desktop.app-folders folder-children "$FC"
	fi
	gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/FFX/ name "FFX"
	gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/FFX/ apps \
		"['firefox-nightly-latest.desktop', 'firefox-devedition-latest.desktop', 'firefox-beta-latest.desktop', 'firefox-latest.desktop', 'firefox-esr-next-latest.desktop', 'firefox-esr-latest.desktop']"
fi

#
# Final
#

echo "[ Info ] Waiting for all downloads to finish"
wait
echo "Success!"
